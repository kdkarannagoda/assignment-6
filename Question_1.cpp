#include<stdio.h>

int triangle(int a);//function prototyping

int main(){
	
	int a=0;//local variable a to main function
	scanf("%i",&a);//asking user to enter the value
	triangle(a);//pushing parameter through the function
	
	return 0;
	
}

int triangle(int a){ //function
	
	for(int i=1;i<a+1;i++){
		
		for(int s=i;s>0;s--){
			
			printf("%i",s);
			
		}
		
		printf("\n");
		
	}
	
}
